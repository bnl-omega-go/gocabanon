module gitlab.cern.ch/bnl-omega-go/gocabanon

go 1.17

require (
	github.com/jpoirier/visa v0.0.0-20180808170255-fcf9fc028c75
	periph.io/x/host/v3 v3.7.2
)

require (
	github.com/codehardt/mmap-go v1.0.1 // indirect
	gitlab.cern.ch/bnl-omega-go/axi v0.0.0-20220219040116-cbda0ec4a509 // indirect
	gitlab.cern.ch/bnl-omega-go/slowcontrol v0.0.0-20220412182123-fbe0b108daa9 // indirect
	golang.org/x/sys v0.0.0-20220209214540-3681064d5158 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b // indirect
	periph.io/x/conn/v3 v3.6.10 // indirect
	periph.io/x/d2xx v0.0.4 // indirect
)
