package main

import (
	"encoding/binary"
	"html/template"
	"log"
	"net/http"
	"strconv"
)

type Parameters struct {
	DAC string
}

func (ctrl *FT232HSPIController) DACValuesUpdate(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("forms_bnl.html"))

	if r.Method != http.MethodPost {
		tmpl.Execute(w, nil)
		return
	}

	values := Parameters{
		DAC: r.FormValue("DAC"),
	}
	D, _ := strconv.ParseInt(values.DAC, 16, 64)
	log.Printf("Received D=%x", D)
	data := make([]byte, 2)
	binary.BigEndian.PutUint16(data, uint16(D))

	ctrl.Write(data)

	tmpl.Execute(w, struct{ Success bool }{true})
}

func main() {
	ps := FT232HSPIController{}
	ps.Init()

	http.HandleFunc("/Configure", ps.DACValuesUpdate)

	http.ListenAndServe(":8080", nil)
}
