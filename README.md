# goCABANON Application


This application , written in go, allow to program the CABANON 32CH Calibration board , see this [link](https://twiki.cern.ch/twiki/bin/viewauth/LAr/LArPhaseIIElectronicsCalibration) for more details. 


## Compilation 

This application relies on [periph.io](https://periph.io) library, and therefore does not have any dependencies in c++. To compile on the machine connected via USB to the CABANON Board, first install the go tool suite on this machine [go install website](https://go.dev/doc/install). This should be compatible with all platforms. 


to compile, clone this repository and execute the following commands from inside the directory :

```
go mod tidy
go build  CABANONTool.go PseudoSPI.go  CLAROCv2.go
go build  CABANONServer.go  PseudoSPI.go  CLAROCv2.go
```

The CABANONTool and CABANONServer executable will be created. 


## usage

The usage of the CABANONTool can be obtains with the -h flag : 

```
./CABANONTool  -h
 ./CABANONTool  -h
Usage of ./CABANONTool:
  -c int
        channel mask (32 bits, 1 bit per channel, lsb is channel 0 (+offset B1,2=48), mapping taken into account) (default 4294967295)
  -d int
        DAC Value (Gain 10 bits LSB) (default 15)
  -fd int
        program the pulser current by providing a 13 bit value, override -t and -d options (default -1)
  -g int
        gain (0-8 => 0-40mA,80mA ...320mA)
  -l int
        run X configurations (default 1)
  -r    perform a final configuration to readback previously written values
  -t int
        Thermometer value (0-7), 3MSB of gain (default 3)
  ```


You can program new values (same for all ASICs), using this command : 

```
./CABANONTool  -c 0b1111 -d 0xB -r -g 0 -t 6
2022/03/17 14:17:06 Found Device, Manufacturer : LAPP, Serial : FTB2, ID : FT, Desc. : CABANON
2022/03/17 14:17:06 Configuring CABANON with Thermometer DAC = 6, Mirrors = 0, DAC = 0xb, Channels = 1111
2022/03/17 14:17:06 Configuring CABANON with Thermometer DAC = 6, Mirrors = 0, DAC = 0xb, Channels = 1111
2022/03/17 14:17:06 Readback value for CABANON : 
2022/03/17 14:17:06 Name : CLB8 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b1111
2022/03/17 14:17:06 Name :  CL7 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b0
2022/03/17 14:17:06 Name : CLB6 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b0
2022/03/17 14:17:06 Name :  CL5 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b0
2022/03/17 14:17:06 Name : CLB4 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b0
2022/03/17 14:17:06 Name :  CL3 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b0
2022/03/17 14:17:06 Name : CLB2 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b0
2022/03/17 14:17:06 Name :  CL1 , ThDAC : 0b111111 , Mirrors : 0b0 , DAC : 0xb , Channels : 0b0
2022/03/17 14:17:06 Readback values as expected!

```

or 

```
./CABANONTool  -g 0 -fd 0xFFF -r -g 1
2022/03/17 14:16:19 Found Device, Manufacturer : LAPP, Serial : FTB2, ID : FT, Desc. : CABANON
2022/03/17 14:16:19 Configuring CABANON with Thermometer DAC = 7, Mirrors = 1, DAC = 0x3ff, Channels = 11111111111111111111111111111111
2022/03/17 14:16:19 Configuring CABANON with Thermometer DAC = 7, Mirrors = 1, DAC = 0x3ff, Channels = 11111111111111111111111111111111
2022/03/17 14:16:19 Readback value for CABANON : 
2022/03/17 14:16:19 Name : CLB8 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Name :  CL7 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Name : CLB6 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Name :  CL5 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Name : CLB4 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Name :  CL3 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Name : CLB2 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Name :  CL1 , ThDAC : 0b1111111 , Mirrors : 0b1 , DAC : 0x3ff , Channels : 0b1111
2022/03/17 14:16:19 Readback values as expected!

```
The channels can be activated using the -c flag with a 32bit integer flag. the LSB of this integer represent the lowest channel number, and MSB the highest. For board B, the MSB is channel 48, for example. 




The CLAROCv2.go and PseudoSPI.go code can be used for more complex examples. 

The CABANONServer is a simple HTTP Server application that serve a form html page on port 8080 (for example http://localhost:8080 , locally). The form can be used to interactively program the CABANON or be contacted by remote code to trigger configuration via the form fields.

To use, run the CABANONServer : 

```
./CABANONServer 
2022/03/15 16:31:22 Found Device, Manufacturer : LAPP, Serial : FTB2, ID : FT, Desc. : CABANON

```

Then connect your browser to http://localhost:8080 , replace localhost by the PC IP address if not connecting from the same PC running the server. 
