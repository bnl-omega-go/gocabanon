package main

import "fmt"

//Interface for CLAROC type devices
type CLAROC interface {
	SetThermometer(value uint8)
	SetChannels(value uint8)
	SetDAC(value uint8)
	SetMirrors(value uint8)
	GetBits() []int
}

//Helper function for Thermometer DAC
func Thermometer(value int) int {
	result := 0
	for i := 0; i < value; i++ {
		result += (1 << i)
	}
	return (result)
}

func InvertBits(value, length uint32) (invval uint32) {

	for i := uint32(0); i < length; i++ {
		if ((value >> i) & 0b1) == 1 {
			invval += 1 << (length - 1 - i)
		}
	}
	return
}

//CLAROC2 Representation
type CLAROCv2 struct {
	name string
	//mapping map[int]int
	CLAROC
	ThermometerDAC, Channels, DAC, Mirrors uint16
}

//Set Functions

func (claroc *CLAROCv2) SetThermometer(value uint16) {
	claroc.ThermometerDAC = value
}

func (claroc *CLAROCv2) SetChannels(value uint16) {
	claroc.Channels = value
}

func (claroc *CLAROCv2) SetDAC(value uint16) {
	claroc.DAC = value
}

func (claroc *CLAROCv2) SetMirrors(value uint16) {
	claroc.Mirrors = value
}

func (claroc *CLAROCv2) String() string {
	return fmt.Sprintf("Name : %4v , ThDAC : 0b%b , Mirrors : 0b%b , DAC : 0x%x , Channels : 0b%b", claroc.name,
		claroc.ThermometerDAC, claroc.Mirrors, claroc.DAC, claroc.Channels)
}

//Function to generate SPI vector for this chip

func (claroc *CLAROCv2) GetBits() []int {

	var index int = 0
	value := make([]int, 29)

	for bit := 0; bit < 7; bit++ {
		value[index] = int((int(claroc.ThermometerDAC) >> bit) & 0b1)
		index++
	}
	for bit := 0; bit < 10; bit++ {
		value[index] = int((claroc.DAC >> bit) & 0b1)
		index++
	}
	for bit := 0; bit < 8; bit++ {
		value[index] = int((claroc.Mirrors >> bit) & 0b1)
		index++
	}
	for bit := 0; bit < 4; bit++ {
		value[index] = int((claroc.Channels >> bit) & 0b1)
		index++
	}
	return value
}

func FillSubDACs(value int) (t, d int) {

	//10bits of DAC
	d = (value) & 0x3FF
	//Thermometer DAC (3 bits eq.)
	tbits := (value >> 10) & 0b111
	for i := 0; i < tbits; i++ {
		t += 1 << i
	}

	return
}
