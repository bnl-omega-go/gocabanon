package main

import (
	"encoding/binary"
	"flag"
	"log"
)

func main() {

	dac := flag.Int("d", 0b0, "DAC Value (16 bits)")
	//verbose := flag.Bool("v", false, "more verbosity")

	flag.Parse()

	ctrl := FT232HSPIController{}
	ctrl.Init()

	data := make([]byte, 2)
	binary.BigEndian.PutUint16(data, uint16(*dac))
	log.Printf("Writing %v (%x) to DAC", *dac, data)
	ctrl.Write(data)
}
