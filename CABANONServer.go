package main

import (
	"html/template"
	"log"
	"net/http"
	"strconv"
)

type Parameters struct {
	TemeratureDAC string
	Mirrors       string
	DAC           string
	Channels      string
}

func (ctrl *PseudoSPIController) CABANONValuesUpdate(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("forms.html"))

	if r.Method != http.MethodPost {
		tmpl.Execute(w, nil)
		return
	}

	values := Parameters{
		TemeratureDAC: r.FormValue("TemperatureDAC"),
		Mirrors:       r.FormValue("Mirrors"),
		DAC:           r.FormValue("DAC"),
		Channels:      r.FormValue("Channels"),
	}
	T, _ := strconv.ParseInt(values.TemeratureDAC, 16, 64)
	M, _ := strconv.ParseInt(values.Mirrors, 16, 64)
	D, _ := strconv.ParseInt(values.DAC, 16, 64)
	C, _ := strconv.ParseInt(values.Channels, 16, 64)
	log.Printf("Received T=%x, M=%x, D=%x, C=%x", T, M, D, C)

	ctrl.ProgramCABANON(Thermometer(int(T)), Thermometer(int(M)), int(D), int(C))

	tmpl.Execute(w, struct{ Success bool }{true})
}

func test(w http.ResponseWriter, r *http.Request) {
	tmpl := template.Must(template.ParseFiles("forms.html"))

	tmpl.Execute(w, struct{ Success bool }{true})

}

func main() {
	ps := PseudoSPIController{}
	ps.Init()

	http.HandleFunc("/Configure", ps.CABANONValuesUpdate)

	http.ListenAndServe(":8080", nil)
}
