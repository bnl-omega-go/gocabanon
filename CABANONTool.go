package main

import (
	"flag"
	"log"
)

func main() {

	channels := flag.Int("c", 0xFFFFFFFF, "channel mask (32 bits, 1 bit per channel, lsb is channel 0 (+offset B1,2=48), mapping taken into account)")
	dac := flag.Int("d", 0b1111, "DAC Value (Gain 10 bits LSB)")
	mirrors := flag.Int("g", 0, "gain (0-8 => 0-40mA,80mA ...320mA)")
	temp := flag.Int("t", 3, "Thermometer value (0-7), 3MSB of gain")
	loop := flag.Int("l", 1, "run X configurations")
	readback := flag.Bool("r", false, "perform a final configuration to readback previously written values")
	FullDAC := flag.Int("fd", -1, "program the pulser current by providing a 13 bit value, override -t and -d options")

	flag.Parse()

	t, m, d, c := 0, 0, 0, 0

	if *mirrors > 8 {
		log.Println("wrong gain value (0-8)")
		return
	}

	if *FullDAC > -1 {
		if *FullDAC > 0x1FFF {
			log.Println("Full DAC Value too large (13bits)")
			return
		}
		c = *channels
		t, d = FillSubDACs(*FullDAC)
		m = Thermometer(*mirrors)

	} else {

		if *dac > 0x3FF {
			log.Println("DAC Value too large (10 bits)")
			return
		}

		if *temp > 7 {
			log.Println("wrong Temperature value (0-7)")
			return
		}
		t = *temp
		d = *dac
		m = Thermometer(*mirrors)
		c = *channels
	}
	ps := PseudoSPIController{}
	ps.Init()

	for i := 0; i < *loop; i++ {

		ps.ProgramCABANON(t, m, d, c)
	}

	if *readback {
		haserrors := ps.ProgramCABANON(t, m, d, c)
		log.Println("Readback value for CABANON : ")
		for _, chip := range clarocs {
			log.Printf("%v", chip.String())
		}
		if haserrors {
			log.Println("Errors detected during readback")
		} else {
			log.Println("Readback values as expected!")
		}
	}
}
