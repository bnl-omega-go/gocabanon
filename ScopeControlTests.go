package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"
	"strconv"
	"time"

	SlowControl "gitlab.cern.ch/bnl-omega-go/slowcontrol"
)

type SCPIer interface {
	Init() error
	ConnectInstrument(name, ipaddr string, port int) error
	Read(name string, length int) (string, error)
	Write(name string, data string) error
	Query(name, request string, length int) (string, error)
	Disconnect(name string)
}

// type VISAController struct {
// 	rm          vi.Session
// 	Instruments map[string]*vi.Object
// }

// func (vic *VISAController) Init() (err error) {
// 	var status vi.Status
// 	vic.rm, status = vi.OpenDefaultRM()
// 	vic.Instruments = make(map[string]*vi.Object)

// 	if status < vi.SUCCESS {
// 		errmsg := fmt.Sprintln("Could not open a session to the VISA Resource Manager!")
// 		err = errors.New(errmsg)
// 	}
// 	defer vic.rm.Close()
// 	return
// }

// func (vic *VISAController) ConnectInstrument(name, ipaddr string, port int) (err error) {
// 	handle := fmt.Sprintf("TCPIP0::%v::%v::SOCKET", ipaddr, port)
// 	instr, status := vic.rm.Open(handle, vi.NULL, vi.NULL)
// 	vic.Instruments[name] = &instr
// 	if status < vi.SUCCESS {
// 		errmsg := fmt.Sprintf("An error occurred opening the session to %v \n ", handle)
// 		err = errors.New(errmsg)
// 	}
// 	return
// }

// func (vic *VISAController) Read(name string, length int) (data string, err error) {
// 	databytes, _, status := vic.Instruments[name].Read(uint32(length))
// 	if status < vi.SUCCESS {
// 		errmsg := fmt.Sprintf("An error occurred during read")
// 		err = errors.New(errmsg)
// 	}
// 	data = string(databytes)
// 	return
// }

// func (vic *VISAController) Write(name string, data string) (err error) {
// 	var status vi.Status
// 	_, status = vic.Instruments[name].Write([]byte(data), uint32(len([]byte(data))))
// 	if status < vi.SUCCESS {
// 		errmsg := fmt.Sprintf("An error occurred during read")
// 		err = errors.New(errmsg)
// 	}
// 	return
// }

// func (vic *VISAController) Query(name string, request string, length int) (data string, err error) {
// 	err = vic.Write(name, request)
// 	if err != nil {
// 		return
// 	}
// 	data, err = vic.Read(name, length)
// 	return
// }

type ScopeController struct {
	ctrl SCPIer
}

func (scope *ScopeController) Init(ipaddr string, port int, controller SCPIer) {
	scope.ctrl = controller
	err := scope.ctrl.Init()
	if err != nil {
		log.Fatal(err)
	}

	err = scope.ctrl.ConnectInstrument("SCOPE", ipaddr, port)
	scope.ctrl.Write("SCOPE", "DCL")

	log.Println("Scope intialized")

	if err != nil {
		log.Fatal(err)
	}
}
func (scope *ScopeController) ID() {
	//scope.ctrl.Write("SCOPE", "*RST")
	log.Println("Seeking scope ID ...")
	data, err := scope.ctrl.Query("SCOPE", "*IDN?", 1)
	//scope.ctrl.Read("SCOPE", 1)

	if err != nil {
		log.Fatal(err)
	}

	log.Println(string(data))
}

func (scope *ScopeController) SetupChannel(base_t float64, base_v float64) (err error) {
	scope.ctrl.Write("SCOPE", "ACQuire:MODe HIRES\n")
	//scope.ctrl.Write("SCOPE", "ACQuire:NUMAVG 100\n")
	scope.ctrl.Write("SCOPE", fmt.Sprintf("HORIZONTAL:SCALE %v\n", base_t))
	for i := 1; i <= 4; i++ {
		scope.ctrl.Write("SCOPE", fmt.Sprintf("CH%v:SCALE %v\n", i, base_v))
		scope.ctrl.Write("SCOPE", fmt.Sprintf("CH%v:TER MEG\n", i))

	}

	return
}

func (scope *ScopeController) SetupTrigger(channel int, level float64, edge string) (err error) {
	scope.ctrl.Write("SCOPE", fmt.Sprintf("TRIGGER:A:EDGE:%v", edge))
	scope.ctrl.Write("SCOPE", fmt.Sprintf("TRIGger:A:EDGE:SOUrce CH%v", channel))
	scope.ctrl.Write("SCOPE", fmt.Sprintf("TRIGger:A:LEVel:CH%v %v", channel, level))
	scope.ctrl.Write("SCOPE", "ACQuire:STOPAfter SEQ")
	return
}

func (scope *ScopeController) GetMeasurements() (m1, m2 float64) {
	scope.ctrl.Write("SCOPE", "MEASU:STATI RESET\n")
	scope.ctrl.Write("SCOPE", "ACQ:STATE RUN\n")
	time.Sleep(10 * time.Second)
	scope.ctrl.Write("SCOPE", "ACQ:STATE STOP\n")

	meas1, _ := scope.ctrl.Query("SCOPE", "MEASUREMENT:MEAS1:MEAN?", 1)
	//scope.ctrl.Read("SCOPE", 1)
	meas2, _ := scope.ctrl.Query("SCOPE", "MEASUREMENT:MEAS2:MEAN?", 1)
	//scope.ctrl.Read("SCOPE", 1)

	m1, _ = strconv.ParseFloat(meas1, 64)
	m2, _ = strconv.ParseFloat(meas2, 64)

	log.Printf("returned results %v , %v", meas1, meas2)
	return
}

func main() {

	scope := ScopeController{}
	//scpictrl := VISAController{}
	scpictrl := SlowControl.SCPIController{}

	ps := PseudoSPIController{}
	ps.Init()

	scope.Init("192.168.0.200", 4000, &scpictrl)
	scope.ID()
	scope.SetupChannel(100e-9, 0.05)
	scope.SetupTrigger(4, 0.6, "RISE")

	f, err := os.Create("Amplitude.csv")
	defer f.Close()

	if err != nil {
		log.Fatalln("failed to open file", err)
	}

	w := csv.NewWriter(f)
	defer w.Flush()
	w.Write([]string{"TEMP", "GAIN", "DAC", "MEAS1", "MEAS2"})
	//ts := []int{0, 0b1, 0b11, 0b111}
	//ts := []int{0b1111}
	for g := 3; g <= 3; g++ {
		for t := 2; t <= 2; t++ {
			for d := 0; d < 1024; d += 1 {
				ps.ProgramCABANON(Thermometer(t), Thermometer(g), d, 0xFFFFFFFF)
				m1, m2 := scope.GetMeasurements()
				fullcode := (g << 13) + (t << 10) + d
				w.Write([]string{fmt.Sprintf("%v", fullcode), fmt.Sprint(g), fmt.Sprint(t), fmt.Sprint(d), fmt.Sprintf("%v", m1), fmt.Sprintf("%v", m2)})
				w.Flush()
				scope.SetupChannel(100e-9, m2*3/10.)

			}
		}
	}

	scope.ctrl.Disconnect("SCOPE")

}
