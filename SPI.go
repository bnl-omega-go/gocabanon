package main

import (
	"log"

	"periph.io/x/conn/v3/physic"
	"periph.io/x/conn/v3/spi"
	"periph.io/x/host/v3"
	"periph.io/x/host/v3/ftdi"
)

type FT232HSPIController struct {
	name   string
	ft232h *ftdi.FT232H
	spi    spi.PortCloser
	conn   spi.Conn
}

func (ctrl *FT232HSPIController) Init() {
	var err error
	if _, err = host.Init(); err != nil {
		log.Fatal(err)
	}

	all := ftdi.All()
	if len(all) == 0 {
		log.Fatal("found no FTDI device on the USB bus")
	}
	var ok bool
	var e ftdi.EEPROM

	for _, v := range all {
		v.EEPROM(&e)
		if e.Desc == "FT232H" {
			log.Printf("Found Device, Manufacturer : %v, Serial : %v, ID : %v, Desc. : %v", e.Manufacturer, e.Serial, e.ManufacturerID, e.Desc)
			ctrl.ft232h = v.(*ftdi.FT232H)
			ok = true
		}
	}

	ctrl.ft232h, ok = all[1].(*ftdi.FT232H)
	ctrl.ft232h.CBus(0b10, 0b10)
	if !ok {
		log.Fatal("not FTDI device on the USB bus")
	}

	ctrl.spi, err = ctrl.ft232h.SPI()
	if err != nil {
		log.Fatal(err)
	}
	ctrl.conn, err = ctrl.spi.Connect(physic.MegaHertz, spi.Mode0, 8)
	if err != nil {
		log.Fatal(err)
	}
}
func (ctrl *FT232HSPIController) Write(data []byte) {
	rd := make([]byte, 2)
	ctrl.conn.Tx(data, rd)
	log.Printf("%x", rd)
}
