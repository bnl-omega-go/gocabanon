package main

import (
	"log"
	"time"

	"periph.io/x/host/v3"
	"periph.io/x/host/v3/ftdi"
)

type PseudoSPIController struct {
	name   string
	ft232h *ftdi.FT232H
}

//Channel mask for FT232H GPIO Pins
const CBUS_MASK = 0b11110
const rstlevel = 1

//Local to Global channel mapping
var CABANON_CH_Mapping [][]int = [][]int{{1, 0, 3, 2}, {5, 4, 7, 6}, {9, 8, 11, 10}, {13, 12, 15, 14},
	{17, 16, 19, 18}, {21, 20, 23, 22}, {25, 24, 27, 26}, {29, 28, 31, 30}}

//Chip position in SPI Chain
var clarocs []CLAROCv2 = []CLAROCv2{{name: "CLB8"}, {name: "CL7"}, {name: "CLB6"}, {name: "CL5"}, {name: "CLB4"}, {name: "CL3"}, {name: "CLB2"}, {name: "CL1"}}

//Function to extract chip channel mask from global mapping
func GetMask(mask uint32, chip int) uint16 {
	chipmap := CABANON_CH_Mapping[chip]
	chipmask := 0
	for i, val := range chipmap {
		status := (int(mask) >> val) & 0b1
		chipmask += status << i
	}
	return uint16(chipmask)
}

func BitsToByte(bits []int) byte {
	var value byte = 0
	for i, val := range bits {
		if val > 0 {
			value += (1 << i)
		}
	}
	return value
}

func BitsToUint32(bits []int) uint32 {
	var value uint32 = 0
	for i, val := range bits {
		if val > 0 {
			value += (1 << i)
		}
	}
	return value
}

//Helper function for SPI Pins with GPIO
func SetSPIPins(clk, datain, load, rstb int) byte {
	return BitsToByte([]int{0, rstb, load, clk, datain, 0, 0, 0})
}

//Initialize FTSDI Controller
func (ctrl *PseudoSPIController) Init() {

	if _, err := host.Init(); err != nil {
		log.Fatal(err)
	}

	all := ftdi.All()

	if len(all) == 0 {
		for trials := 0; trials < 10; trials++ {
			time.Sleep(100 * time.Millisecond)
			all = ftdi.All()
			log.Println(all)
			if len(all) > 0 {
				break
			}
		}
		log.Fatal("found no FTDI device on the USB bus")
	}

	var ok bool
	var e ftdi.EEPROM

	for _, v := range all {
		v.EEPROM(&e)
		if e.Desc == "CABANON" {
			log.Printf("Found Device, Manufacturer : %v, Serial : %v, ID : %v, Desc. : %v", e.Manufacturer, e.Serial, e.ManufacturerID, e.Desc)
			ctrl.ft232h = v.(*ftdi.FT232H)
			ok = true
		}
	}

	if !ok {
		log.Fatal("not FTDI device on the USB bus")
	}

	ctrl.name = e.Desc

	err := ctrl.ft232h.CBus(CBUS_MASK, 0x0)
	if err != nil {
		log.Fatal(err)
	}
	//log.Println(ctrl.ft232h.Header())
}

//Get configuration bit list and send it to board via FT232H Bitbanging
func (ctrl *PseudoSPIController) Configure(bits []int) []int {

	result := make([]int, 0)
	var i, bit int
	for i, bit = range bits {
		//LOWER clock
		err := ctrl.ft232h.CBus(CBUS_MASK, SetSPIPins(0, bit, 0, rstlevel))
		if err != nil {
			log.Fatal(err)
		}

		//Read bit
		readbyte, err := ctrl.ft232h.CBusRead()
		if err != nil {
			log.Fatal(err)
		}
		if i != -1 {
			result = append(result, int((readbyte)&0b1))
			//log.Printf("Read byte %b", readbyte)
		}

		//write data, raise clock
		err = ctrl.ft232h.CBus(CBUS_MASK, SetSPIPins(1, bit, 0, rstlevel))
		if err != nil {
			log.Fatal(err)
		}

	}

	//latch
	err := ctrl.ft232h.CBus(CBUS_MASK, SetSPIPins(0, bit, 1, rstlevel))
	if err != nil {
		log.Fatal(err)
	}

	//Unlatch
	err = ctrl.ft232h.CBus(CBUS_MASK, SetSPIPins(0, 0, 0, rstlevel))
	if err != nil {
		log.Fatal(err)
	}

	return result

}

//Top level function, receive mask for 32 CH and values for ASIC for DAC, ThermometerDAc and Mirrors , generate bits and configure via FT232H
func (ps *PseudoSPIController) ProgramCABANON(temp, mirrors, dac, channels int) bool {

	bitsarray := make([]int, 0)

	sent := make([]uint32, 8)
	recvd := make([]uint32, 8)

	haserrors := false

	for i, chip := range clarocs {
		chip.SetChannels(GetMask(uint32(channels), i))
		chip.SetDAC(uint16(dac))
		chip.SetMirrors(uint16(mirrors))
		chip.SetThermometer(uint16(Thermometer(temp)))
		cb := chip.GetBits()
		sent[i] = BitsToUint32(cb)
		log.Printf("Sent bits: %v", cb)
		bitsarray = append(bitsarray, cb...)
	}

	log.Printf("Configuring CABANON with Thermometer DAC = %b, Mirrors = %b, DAC = 0x%x, Channels = %b", temp, mirrors, dac, channels)
	result := ps.Configure(bitsarray)
	//log.Printf("Read back : length %v , %v", len(result), result)

	//merge bits
	for i, _ := range clarocs {
		var fullreg uint32 = 0

		for j, bit := range result[i*29 : (i+1)*29] {
			if bit == 1 {
				fullreg += 1 << (28 - j)
			}
		}
		recvd[i] = fullreg

		if sent[i] != InvertBits(recvd[i], 29) {
			//log.Printf("Error in readback %b!=%b", sent[i], InvertBits(recvd[i], 29))
			haserrors = true
		}

		ch := InvertBits((fullreg)&0b1111, 4)
		mr := InvertBits((fullreg>>4)&0xFF, 8)
		d := InvertBits((fullreg>>12)&0x3FF, 10)
		T := InvertBits((fullreg>>22)&0x7FF, 7)

		clarocs[i].SetChannels(uint16(ch))
		clarocs[i].SetMirrors(uint16(mr))
		clarocs[i].SetDAC(uint16(d))
		clarocs[i].SetThermometer(uint16(T))
	}
	return haserrors
}
